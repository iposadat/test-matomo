#!/bin/bash
set -e

echo "--> Setting up configurations ..."
envsubst < /tmp/config/matomo-config/config.ini.php > /var/www/html/config/config.ini.php
echo "--> Setting up configurations ... [DONE]"

# Enabling extra plugins
# This is specially needed when a plugin requires to create
# specific databases.
php console plugin:activate LoginOIDC
php console plugin:activate LogViewer

# Update Matomo database in case there is a new version.
php console core:update --yes
