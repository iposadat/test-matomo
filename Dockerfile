FROM matomo:4.8.0-apache

# Check https://github.com/digitalist-se/extratools/tags for newer releases
ENV \
  PLUGIN_LOGVIEWER_VERSION=4.0.1 \
  PLUGIN_LOGINOIDC_VERSION=4.x-dev

RUN \
    # moreutils is needed so that we can install sponge tool,
    # needed for envsubst variable replacement.
    apt-get update && apt-get install -y vim gettext unzip moreutils \
    # For bootstrap purposes
    default-mysql-client && \

#
# Install plugins
#
    curl -L -o LogViewer.zip \
    https://github.com/matomo-org/plugin-LogViewer/archive/refs/tags/${PLUGIN_LOGVIEWER_VERSION}.zip \
    && unzip LogViewer.zip \
    && rm LogViewer.zip \
    && mv plugin-LogViewer-${PLUGIN_LOGVIEWER_VERSION} /usr/src/matomo/plugins/LogViewer && \

    curl -L -o LoginOIDC.zip \
    https://github.com/dominik-th/matomo-plugin-LoginOIDC/archive/refs/heads/${PLUGIN_LOGINOIDC_VERSION}.zip \
    && unzip LoginOIDC.zip \
    && rm LoginOIDC.zip \
    && mv matomo-plugin-LoginOIDC-${PLUGIN_LOGINOIDC_VERSION} /usr/src/matomo/plugins/LoginOIDC && \
    
#
# PHP config
#
    sed -i "s|max_execution_time = 30|max_execution_time = 360|g" /usr/local/etc/php/php.ini-production && \
    cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini && \

#
# Apache2 config
#
    sed -i "s|Listen 80|Listen 8080|g" /etc/apache2/ports.conf && \
    # Enable mods
    a2enmod headers log_debug && \
    # Grant permissions to random uid user in order to inject extra configuration
    chgrp -R 0 /etc/apache2/conf-available && chmod -R g=u /etc/apache2/conf-available && \
    chgrp -R 0 /etc/apache2/conf-enabled && chmod -R g=u /etc/apache2/conf-enabled && \
    # Needed when enabling a2enconf configurations, since enabled configuration will land in here
    chgrp -R 0 /var/lib/apache2/conf/enabled_by_admin && chmod -R g=u /var/lib/apache2/conf/enabled_by_admin && \

#
# Setup Matomo - copy Matomo files over /var/www/html
#
    tar cf - --one-file-system -C /usr/src/matomo . | tar xf - && \
    chgrp -R 0 /var/www/html && chmod -R g=u /var/www/html

#
# Copy run scripts for both Apache and DB migration
#
COPY run-apache.sh init-container.sh /
RUN chmod +x /run-apache.sh /init-container.sh

CMD ["sh","/run-apache.sh"]
