#!/bin/bash
set -e

# Replace environment variables
echo "--> Setting up configurations ..."
envsubst < /tmp/config/matomo-config/config.ini.php > /var/www/html/config/config.ini.php
echo "--> Setting up configurations ... [DONE]"

# envsubst < /tmp/matomo-config/httpdcern10.conf > /etc/apache2/conf-available/httpdcern10.conf

# Enabling Apache2 configuration
# a2enconf httpdcern10

exec apache2-foreground
